﻿using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;

namespace MobileServer
{
    public class CHECK_VERSION : TcpCommand
    {
        public CHECK_VERSION(SqlConnection inConnection) : base(inConnection) { }
        public override string GetResult()
        {
            return FileVersionInfo.GetVersionInfo(Application.ProductName + ".exe").FileVersion;
        }
    }
}
