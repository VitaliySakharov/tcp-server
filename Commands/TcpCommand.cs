﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data;

namespace MobileServer
{
    public abstract class TcpCommand
    {
        protected SqlConnection Connection { get; }
        public string ClientID { get; set; }
        public string Command { get; set; }
        public string[] Params { get; set; }

        public TcpCommand(SqlConnection inConnection)
        {
            Connection = inConnection;
        }
        public abstract string GetResult();
    }
}