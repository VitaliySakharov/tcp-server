﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MobileServer
{
    public class INSERT : TcpCommand
    {
        public INSERT(SqlConnection inConnection) : base(inConnection) { }
        public override string GetResult()
        {
            string result = "";

            using (SqlCommand sql = Connection.CreateCommand())
            {
                sql.CommandText = "sp_Change";
                sql.CommandType = CommandType.StoredProcedure;

                sql.Parameters.Add(new SqlParameter("@TypeOper", "I"));
                sql.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int) { Direction = ParameterDirection.Output });
                sql.Parameters.Add(new SqlParameter("@Begin", Convert.ToDateTime(DateTime.Now.ToString("dd.MM.yyyy HH:mm"))));
                sql.Parameters.Add(new SqlParameter("@Amount", 1));

                sql.ExecuteNonQuery();
                result = Convert.ToString(sql.Parameters["@ID"].Value);
            }

            return result;
        }
    }
}
