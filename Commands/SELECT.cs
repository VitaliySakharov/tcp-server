﻿using System;
using System.Data.SqlClient;

namespace MobileServer
{
    public class SELECT : TcpCommand
    {
        public SELECT(SqlConnection inConnection) : base(inConnection) { }
        public override string GetResult()
        {
            switch (Params[0])
            {
                case "Services":
                    return GetSQLResult(Services());
                case "Tasks":
                    return GetSQLResult(Tasks());
                default:
                    return new Random(10).Next().ToString();
            }
        }
        private string GetSQLResult(SqlCommand cmd)
        {
            string result = "";

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read()) // построчно считываем данные
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            result += reader.GetValue(i) + ";";
                        }
                        result += "#";
                    }
                }
                reader.Close();
            }
            return result.Replace("\r\n", " ");
        }
        private SqlCommand Services()
        {
            using (SqlCommand sql = Connection.CreateCommand())
            {
                sql.CommandText = "select * from fnGetService(@OA_ID, @Device_ID) ";
                sql.Parameters.Add(new SqlParameter("@Device_ID", ClientID));

                return sql;
            }
        }
        private SqlCommand Tasks()
        {
            using (SqlCommand sql = Connection.CreateCommand())
            {
                sql.CommandText = "select * from fnGetTasks(@Device_ID) order by 1";
                sql.Parameters.Add(new SqlParameter("@Device_ID", ClientID));
                return sql;
            }
        }
    }
}
