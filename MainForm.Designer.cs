﻿namespace MobileServer
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbNet = new System.Windows.Forms.CheckBox();
            this.cbXML = new System.Windows.Forms.CheckBox();
            this.cbMail = new System.Windows.Forms.CheckBox();
            this.cbModify = new System.Windows.Forms.CheckBox();
            this.cbReg = new System.Windows.Forms.CheckBox();
            this.cbSelect = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbMail = new System.Windows.Forms.TextBox();
            this.btMailTest = new System.Windows.Forms.Button();
            this.tbPortMail = new System.Windows.Forms.TextBox();
            this.tbHostMail = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbPortESB = new System.Windows.Forms.TextBox();
            this.tbHostESB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbHostAODB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbHostTCP = new System.Windows.Forms.TextBox();
            this.btServer = new System.Windows.Forms.Button();
            this.tbPortTCP = new System.Windows.Forms.TextBox();
            this.tbLog = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1222, 112);
            this.panel1.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbNet);
            this.groupBox4.Controls.Add(this.cbXML);
            this.groupBox4.Controls.Add(this.cbMail);
            this.groupBox4.Controls.Add(this.cbModify);
            this.groupBox4.Controls.Add(this.cbReg);
            this.groupBox4.Controls.Add(this.cbSelect);
            this.groupBox4.Location = new System.Drawing.Point(706, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(148, 89);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Лог";
            // 
            // cbNet
            // 
            this.cbNet.AutoSize = true;
            this.cbNet.Location = new System.Drawing.Point(6, 63);
            this.cbNet.Name = "cbNet";
            this.cbNet.Size = new System.Drawing.Size(43, 17);
            this.cbNet.TabIndex = 13;
            this.cbNet.Text = "Net";
            this.cbNet.UseVisualStyleBackColor = true;
            this.cbNet.CheckedChanged += new System.EventHandler(this.cbNet_CheckedChanged);
            // 
            // cbXML
            // 
            this.cbXML.AutoSize = true;
            this.cbXML.Location = new System.Drawing.Point(71, 63);
            this.cbXML.Name = "cbXML";
            this.cbXML.Size = new System.Drawing.Size(48, 17);
            this.cbXML.TabIndex = 12;
            this.cbXML.Text = "XML";
            this.cbXML.UseVisualStyleBackColor = true;
            this.cbXML.CheckedChanged += new System.EventHandler(this.cbXML_CheckedChanged);
            // 
            // cbMail
            // 
            this.cbMail.AutoSize = true;
            this.cbMail.Location = new System.Drawing.Point(71, 40);
            this.cbMail.Name = "cbMail";
            this.cbMail.Size = new System.Drawing.Size(51, 17);
            this.cbMail.TabIndex = 11;
            this.cbMail.Text = "eMail";
            this.cbMail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbMail.UseVisualStyleBackColor = true;
            this.cbMail.CheckedChanged += new System.EventHandler(this.cbMail_CheckedChanged);
            // 
            // cbModify
            // 
            this.cbModify.AutoSize = true;
            this.cbModify.Location = new System.Drawing.Point(71, 17);
            this.cbModify.Name = "cbModify";
            this.cbModify.Size = new System.Drawing.Size(57, 17);
            this.cbModify.TabIndex = 10;
            this.cbModify.Text = "Modify";
            this.cbModify.UseVisualStyleBackColor = true;
            this.cbModify.CheckedChanged += new System.EventHandler(this.cbModify_CheckedChanged);
            // 
            // cbReg
            // 
            this.cbReg.AutoSize = true;
            this.cbReg.Location = new System.Drawing.Point(6, 17);
            this.cbReg.Name = "cbReg";
            this.cbReg.Size = new System.Drawing.Size(46, 17);
            this.cbReg.TabIndex = 9;
            this.cbReg.Text = "Reg";
            this.cbReg.UseVisualStyleBackColor = true;
            this.cbReg.CheckedChanged += new System.EventHandler(this.cbReg_CheckedChanged);
            // 
            // cbSelect
            // 
            this.cbSelect.AutoSize = true;
            this.cbSelect.Location = new System.Drawing.Point(6, 40);
            this.cbSelect.Name = "cbSelect";
            this.cbSelect.Size = new System.Drawing.Size(56, 17);
            this.cbSelect.TabIndex = 8;
            this.cbSelect.Text = "Select";
            this.cbSelect.UseVisualStyleBackColor = true;
            this.cbSelect.CheckedChanged += new System.EventHandler(this.cbSelect_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbMail);
            this.groupBox3.Controls.Add(this.btMailTest);
            this.groupBox3.Controls.Add(this.tbPortMail);
            this.groupBox3.Controls.Add(this.tbHostMail);
            this.groupBox3.Location = new System.Drawing.Point(453, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(247, 89);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "EMail";
            // 
            // tbMail
            // 
            this.tbMail.Location = new System.Drawing.Point(12, 58);
            this.tbMail.Name = "tbMail";
            this.tbMail.ReadOnly = true;
            this.tbMail.Size = new System.Drawing.Size(228, 20);
            this.tbMail.TabIndex = 15;
            // 
            // btMailTest
            // 
            this.btMailTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btMailTest.Location = new System.Drawing.Point(180, 24);
            this.btMailTest.Name = "btMailTest";
            this.btMailTest.Size = new System.Drawing.Size(60, 21);
            this.btMailTest.TabIndex = 11;
            this.btMailTest.Text = "Test";
            this.btMailTest.UseVisualStyleBackColor = true;
            this.btMailTest.Click += new System.EventHandler(this.btMailTest_Click);
            // 
            // tbPortMail
            // 
            this.tbPortMail.Location = new System.Drawing.Point(133, 25);
            this.tbPortMail.Name = "tbPortMail";
            this.tbPortMail.ReadOnly = true;
            this.tbPortMail.Size = new System.Drawing.Size(41, 20);
            this.tbPortMail.TabIndex = 14;
            // 
            // tbHostMail
            // 
            this.tbHostMail.Location = new System.Drawing.Point(12, 25);
            this.tbHostMail.Name = "tbHostMail";
            this.tbHostMail.ReadOnly = true;
            this.tbHostMail.Size = new System.Drawing.Size(115, 20);
            this.tbHostMail.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbPortESB);
            this.groupBox1.Controls.Add(this.tbHostESB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbHostAODB);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(228, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 89);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Database";
            // 
            // tbPortESB
            // 
            this.tbPortESB.Location = new System.Drawing.Point(170, 58);
            this.tbPortESB.Name = "tbPortESB";
            this.tbPortESB.ReadOnly = true;
            this.tbPortESB.Size = new System.Drawing.Size(41, 20);
            this.tbPortESB.TabIndex = 6;
            // 
            // tbHostESB
            // 
            this.tbHostESB.Location = new System.Drawing.Point(49, 58);
            this.tbHostESB.Name = "tbHostESB";
            this.tbHostESB.ReadOnly = true;
            this.tbHostESB.Size = new System.Drawing.Size(115, 20);
            this.tbHostESB.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ESB";
            // 
            // tbHostAODB
            // 
            this.tbHostAODB.Location = new System.Drawing.Point(49, 25);
            this.tbHostAODB.Name = "tbHostAODB";
            this.tbHostAODB.ReadOnly = true;
            this.tbHostAODB.Size = new System.Drawing.Size(115, 20);
            this.tbHostAODB.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "AODB";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbHostTCP);
            this.groupBox2.Controls.Add(this.btServer);
            this.groupBox2.Controls.Add(this.tbPortTCP);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(210, 89);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " TCP server ";
            // 
            // tbHostTCP
            // 
            this.tbHostTCP.Location = new System.Drawing.Point(6, 25);
            this.tbHostTCP.Name = "tbHostTCP";
            this.tbHostTCP.ReadOnly = true;
            this.tbHostTCP.Size = new System.Drawing.Size(115, 20);
            this.tbHostTCP.TabIndex = 5;
            // 
            // btServer
            // 
            this.btServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btServer.Location = new System.Drawing.Point(129, 54);
            this.btServer.Name = "btServer";
            this.btServer.Size = new System.Drawing.Size(75, 23);
            this.btServer.TabIndex = 3;
            this.btServer.Text = "Start";
            this.btServer.UseVisualStyleBackColor = true;
            this.btServer.Click += new System.EventHandler(this.btServer_Click);
            // 
            // tbPortTCP
            // 
            this.tbPortTCP.Location = new System.Drawing.Point(6, 56);
            this.tbPortTCP.Name = "tbPortTCP";
            this.tbPortTCP.ReadOnly = true;
            this.tbPortTCP.Size = new System.Drawing.Size(115, 20);
            this.tbPortTCP.TabIndex = 2;
            // 
            // tbLog
            // 
            this.tbLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLog.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbLog.Location = new System.Drawing.Point(0, 0);
            this.tbLog.MaxLength = 100;
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(1214, 534);
            this.tbLog.TabIndex = 3;
            this.tbLog.Text = "";
            this.tbLog.WordWrap = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 112);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1222, 560);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.tbLog);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1214, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Лог команд";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1222, 672);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "Mobile server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbPortTCP;
        private System.Windows.Forms.Button btServer;
        private System.Windows.Forms.RichTextBox tbLog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbHostESB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbHostAODB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPortESB;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbMail;
        private System.Windows.Forms.Button btMailTest;
        private System.Windows.Forms.TextBox tbPortMail;
        private System.Windows.Forms.TextBox tbHostMail;
        private System.Windows.Forms.TextBox tbHostTCP;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox cbSelect;
        private System.Windows.Forms.CheckBox cbReg;
        private System.Windows.Forms.CheckBox cbModify;
        private System.Windows.Forms.CheckBox cbMail;
        private System.Windows.Forms.CheckBox cbXML;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox cbNet;
    }
}

