﻿using System;
using System.Data.SqlClient;

namespace MobileServer
{
    static class CommandsSimpleFactory
    {
        private static void ParseRequest(string inRequest, out string inOutCommand, out string[] inOutParams)
        {
            string[] inArray = inRequest.TrimEnd('#').Split(';');

            if (inArray.Length >= 1)
                inOutCommand = inArray[0];
            else
                inOutCommand = "";

            if (inArray.Length >= 1)
                inOutParams = inRequest.Substring(inArray[0].Length + 1).TrimEnd('#').Split(';');
            else
                inOutParams = new string[0];
        }
        public static TcpCommand GetCommand(string inRequest, SqlConnection inConnection)
        {
            ParseRequest(inRequest, out string Command, out string[] Params);

            Type CommandType = Type.GetType("MobileServer." + Command, true, true);

            TcpCommand Cmd = Activator.CreateInstance(CommandType, inConnection) as TcpCommand;

            Cmd.ClientID = new Random(100).Next().ToString();
            Cmd.Command = Command;
            Cmd.Params = Params;

            return Cmd;
        }
    }
}
