﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace MobileServer
{
    public class TcpServer
    {
        public static string connectionString =
        "Persist Security Info=True" +
        ";Initial Catalog=" +
        ";User ID=" +
        ";Password=" +
        ";Integrated Security=false" +
        ";Connection Timeout = 1" +
        ";Server=" + ConfigurationManager.AppSettings["hostAODB"];

        bool bStop = false;
        TcpListener listener;
        private RichTextBox tb;

        public RichTextBox LogTextBox
        {
            get { return tb; }
            set { tb = value; }
        }

        public async void Stop()
        {
            if (!listener.Pending())
            {
                //for stoping started method "await listener.AcceptTcpClientAsync" without exception
                using (TcpClient closingClient = new TcpClient())
                {
                    bStop = true;
                    await closingClient.ConnectAsync((listener.LocalEndpoint as IPEndPoint).Address, (listener.LocalEndpoint as IPEndPoint).Port);
                    closingClient.Close();
                }
            }

            if (listener != null)
            {
                listener.Stop();
                listener = null;
            }
        }
        public async void Start(IPAddress ip, int port)
        {
            listener = new TcpListener(ip, port);
            listener.ExclusiveAddressUse = false;
            try
            {
                listener.Start();
            }
            catch (Exception e)
            {
                PrintLog(null, null, e.Message, null, Color.Red);
                return;
            }

            while (!bStop)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                Task t = ProcessConnection(client);
            }
        }

        private Task ProcessConnection(TcpClient client)
        {
            return Task.Run(() =>
            {
                Stopwatch logStart = new Stopwatch();
                logStart.Start();

                string clientIP = Convert.ToString(((IPEndPoint)client.Client.RemoteEndPoint).ToString());

                string request = "";
                string readError = "";
                TcpCommand cmd = null;
                SqlConnection db = new SqlConnection(connectionString);
                string result = "";

                try
                {
                    request = ReadLnKeep(client, 1024, out readError);

                    if (request == "")
                    {
                        PrintLog(clientIP, null, "ReadLn error: {" + readError + "}", logStart, Color.Purple);
                        return "";
                    }

                    cmd = CommandsSimpleFactory.GetCommand(request, db);

                    db.Open();

                    result = cmd.GetResult();
                    WriteLn(client, result);

                    PrintLog(clientIP, cmd, result, logStart, Color.Black);
                }
                catch (Exception e)
                {
                    string err = "{" + request + "} -> " + e.Message + "\r\n" + e.StackTrace;
                    while (e.InnerException != null)
                    {
                        err += "\r\n" + e.InnerException.Message + "\r\n" + e.InnerException.StackTrace;
                        e = e.InnerException;
                    }

                    PrintLog(clientIP, null, err, logStart, Color.Red);
                }
                finally
                {
                    logStart.Stop();

                    if (db != null)
                        db.Close();

                    db = null;
                    cmd = null;
                    client.Close();
                }
                return "";
            });
        }

        private static void WriteLn(TcpClient client, string msgString)
        {
            byte[] buffer;
            NetworkStream stream = null;

            msgString += "\r\n";

            try
            {
                buffer = Encoding.UTF8.GetBytes(msgString);
                stream = client.GetStream();
                stream.Write(buffer, 0, buffer.Length);
            }
            finally
            {
                stream = null;
                buffer = null;
            }
        }
        private static string ReadLnKeep(TcpClient client, int bufferlength, out string error)
        {
            error = "";

            string result = "";
            byte[] buffer = new byte[bufferlength];
            NetworkStream stream = null;
            MemoryStream memStream = new MemoryStream();

            try
            {
                stream = client.GetStream();
                stream.ReadTimeout = 30000;

                while (!result.Contains("\r\n"))
                {
                    try
                    {
                        int count = stream.Read(buffer, 0, buffer.Length);
                        if (count == 0)
                            break;

                        memStream.Write(buffer, 0, count);
                        result = Encoding.UTF8.GetString(memStream.ToArray());
                    }
                    catch (Exception e)
                    {
                        var w32ex = e.InnerException as System.ComponentModel.Win32Exception;

                        if (w32ex != null && w32ex.NativeErrorCode.Equals(10060))
                            error = w32ex.ErrorCode + " Превышено время ожидания запроса";
                        else if (w32ex != null)
                            error = w32ex.ErrorCode + " " + w32ex.Message;
                        else
                            error = "Ошибка при попытке чтения " + e.Message;
                        break;
                    }
                }

                if (!result.Contains("\r\n") && error == "")
                    error = "Соединение разорвано";

                if (result.IndexOf("\r\n") > 0)
                    result = result.Substring(0, result.IndexOf("\r\n"));
                else
                    result = result.Replace("\r\n", "");
            }
            catch (Exception e)
            {
                throw new Exception("ReadLn error: ", e);
            }
            finally
            {
                stream = null;
                buffer = null;
            }

            return result;
        }

        private void PrintLog(string cIP, TcpCommand cmd, string res, Stopwatch dBegin, Color color)
        {
            if (tb == null) return;

            string str = "";

            if (dBegin != null)
                str = DateTime.Now.ToString("dd MMM HH:mm:ss") + "   ~" + dBegin.ElapsedMilliseconds.ToString("0:000") + "   " + cIP.PadRight(21, '\u2002') + "   ";

            if (cmd == null)
                str += res;
            else
                str += cmd.ClientID.PadRight(29, '\u2002') + "   " + cmd.Command + ";" + cmd.Params + "  ->  " + res;


            tb.Invoke(new Action(() =>
            {
                int start = tb.TextLength;
                tb.AppendText("\r\n" + str);
                int end = tb.TextLength;

                tb.Select(start, end - start);
                tb.SelectionColor = color;

                tb.SelectionLength = 0;
            }));

        }
    }

}

