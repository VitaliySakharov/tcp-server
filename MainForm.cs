﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.Reflection;

namespace MobileServer
{
    public partial class MainForm : Form
    {   
        TcpServer server;                

        public MainForm() => InitializeComponent();        

        private void btServer_Click(object sender, EventArgs e)
        {
            if (server == null)
            {
                server = new TcpServer();
                server.LogTextBox = tbLog;
                server.Start(IPAddress.Parse(ConfigurationManager.AppSettings["hostTCP"]), Convert.ToInt32(ConfigurationManager.AppSettings["portTCP"])); 
                btServer.Text = "Stop";
            }
            else
            {
                server.Stop();
                server.LogTextBox = null;
                server = null;
                btServer.Text = "Start";
                tbLog.AppendText(Environment.NewLine + "Сервер приложений остановлен"); 
            }
        }
        
        private void MainForm_Shown(object sender, EventArgs e)
        {
            //SqlDependency.Start(connectionString);            

            this.Text += "  v" + FileVersionInfo.GetVersionInfo(Application.ProductName + ".exe").FileVersion
                      + "  (MobApp v" + ConfigurationManager.AppSettings["mobAppVer"] + ")";

            tbHostTCP.Text = ConfigurationManager.AppSettings["hostTCP"];
            tbPortTCP.Text = ConfigurationManager.AppSettings["portTCP"];


            try
            {
                string f = File.ReadAllText("TechCard.txt");
            }
            catch { tbLog.AppendText(Environment.NewLine + "Файл тех.карты не найден"); }

            try
            {
                byte[] f = File.ReadAllBytes("Sign.jpg");
            }
            catch { tbLog.AppendText(Environment.NewLine + "Файл подписи не найден"); }

            try
            {
                string f = File.ReadAllText("BagBelt.xml");
            }
            catch { tbLog.AppendText(Environment.NewLine + "Файл BagBelt.xml не найден"); }

            try
            {
                using (SqlConnection db = new SqlConnection(TcpServer.connectionString))
                {
                    db.Open();
                }
            }
            catch (Exception ex)
            {
                tbLog.AppendText(Environment.NewLine + "Не удалось подключиться к БД: " + ex.Message);
            }

            btServer_Click(this, null);
        }
        private void btMailTest_Click(object sender, EventArgs e)
        {
            //tbLog.AppendText(Environment.NewLine + "Статус отправки письма: " + MAIL.MailResult("", "Тест", "Тестовое письмо", null));
        }
        private void cbSelect_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSelect.Checked)
                ConfigurationManager.AppSettings.Set("printSelect", "1");
            else
                ConfigurationManager.AppSettings.Set("printSelect", "0");
        }
        private void cbReg_CheckedChanged(object sender, EventArgs e)
        {
            if (cbReg.Checked)
                ConfigurationManager.AppSettings.Set("printReg", "1");
            else
                ConfigurationManager.AppSettings.Set("printReg", "0");
        }
        private void cbModify_CheckedChanged(object sender, EventArgs e)
        {
            if (cbModify.Checked)
                ConfigurationManager.AppSettings.Set("printModify", "1");
            else
                ConfigurationManager.AppSettings.Set("printModify", "0");
        }
        private void cbMail_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMail.Checked)
                ConfigurationManager.AppSettings.Set("printMail", "1");
            else
                ConfigurationManager.AppSettings.Set("printMail", "0");
        }
        private void cbNet_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNet.Checked)
                ConfigurationManager.AppSettings.Set("printNet", "1");
            else
                ConfigurationManager.AppSettings.Set("printNet", "0");
        }
        private void cbXML_CheckedChanged(object sender, EventArgs e)
        {

        }
        
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //SqlDependency.Stop(connectionString);
            btServer_Click(this, null);
        }

    }   

}
